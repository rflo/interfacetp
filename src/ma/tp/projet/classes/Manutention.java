package ma.tp.projet.classes;

import java.util.List;

public class Manutention extends Employe {

	private int nbHeures;
	private final static double SALAIRE_HORAIRE = 65.00;
	
	
	public double calculerSalaire() {
		
		return nbHeures * SALAIRE_HORAIRE;
	}


	public Manutention(String nom, String prenom, int age, String dateEntree, int nbHeures) {
		super(nom, prenom, age, dateEntree);
		this.nbHeures = nbHeures;
	}
	
	public String getTitre() {
	return "Le manutentionnaire ";
	}
}