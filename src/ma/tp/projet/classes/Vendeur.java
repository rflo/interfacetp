package ma.tp.projet.classes;

import java.util.List;

public class Vendeur extends Employe {

	double chiffreAffaire;

	public double getChiffreAffaire() {
		return chiffreAffaire;
	}

	public void setChiffreAffaire(double chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}

	@Override
	public String toString() {
		return "Vendeur [chiffreAffaire=" + chiffreAffaire + "]";
	}

	public Vendeur(String nom, String prenom, int age, String dateEntree, double chiffreAffaire) {
		super(nom, prenom, age, dateEntree);
		this.chiffreAffaire = chiffreAffaire;
	}

	public Vendeur(String nom, String prenom, int age, String dateEntree) {
		super(nom, prenom, age, dateEntree);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double calculerSalaire() {
		
		return (chiffreAffaire * 20 / 100) + 400;
	}
	
	
}