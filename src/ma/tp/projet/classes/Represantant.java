package ma.tp.projet.classes;

import java.util.List;

public abstract class Represantant extends Employe {

	private double chiffreAffaire;

	public double getChiffreAffaire() {
		return chiffreAffaire;
	}

	public void setChiffreAffaire(double chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}

	@Override
	public String toString() {
		return "Representant [chiffreAffaire=" + chiffreAffaire + "]";
	}

	public Represantant(String nom, String prenom, int age, String dateEntree, double chiffreAffaire) {
		super(nom, prenom, age, dateEntree);
		this.chiffreAffaire = chiffreAffaire;
	}

	public Represantant(String nom, String prenom, int age, String dateEntree) {
		super(nom, prenom, age, dateEntree);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double calculerSalaire() {
		
		return (chiffreAffaire * 20 / 100) + 800;
	}
	
	
}